RASA_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

DATA = "data"
RESULTS = "results"

# API_ENDPOINT = "api_endpoint"
KEY = "key"
RASA_TOKEN = "rasa-token"
COMMUNITY_ID = "community_id"
TYPE = "type"

# # Person fields
ID = "id"
EXTERNAL_ID = "external_id"
EMAIL = "email"
FIRST_NAME = "first_name"
LAST_NAME = "last_name"
UPDATED = "updated"
IS_SUBSCRIBED = "is_subscribed"
UNSUBSCRIBE_REASON = "unsubscribe_reason"
IS_ACTIVE = "is_active"
IS_ARCHIVED = "is_archived"
STATUS = "status"
CREATED = "created"
